'use strict';  // js strict mode: catch undeclared variables
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
	console.log("Create component");
  }

  tick() {
	console.log("ticking");
    this.setState({
      date: new Date()
    });
  }
  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      3000
    );
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  render() {
	console.log("Created element");
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }

}  // closes class declaration
ReactDOM.render(
  <Clock />,
  document.getElementById('clock-container')
);
